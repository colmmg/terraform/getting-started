provider "aws" {
  region = "us-east-1"
}

data "aws_caller_identity" "current" {}

data "terraform_remote_state" "stack-1-dynamodb-table" {
  backend = "s3"
  config = {
    bucket = "terraform-state-bucket-${data.aws_caller_identity.current.account_id}-dev"
    key    = "stack-1-dynamodb-table/terraform.tfstate"
    region = "us-east-1"
  }
}

resource "aws_dynamodb_table_item" "my-table-item" {
  table_name = data.terraform_remote_state.stack-1-dynamodb-table.outputs.my-table-name
  hash_key   = data.terraform_remote_state.stack-1-dynamodb-table.outputs.my-table-hash-key

  item = <<ITEM
{
  "AgentId": {"N": "007"},
  "FirstName": {"S": "James"},
  "LastName": {"S": "Bond"}
}
ITEM
}
