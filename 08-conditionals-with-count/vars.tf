variable "create-ebs-volume" {
  description = "A boolean to inidcate whether or not the EBS volume should be created."
}

variable "size" {
  description = "The size of the EBS volume."
}
