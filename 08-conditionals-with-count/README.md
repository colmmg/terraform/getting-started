# Introduction
You can use [Terraform Expressions](https://www.terraform.io/docs/configuration/expressions.html) to apply conditional logic to your code. Specifically, you may use the `count` expression to do this.

# Boolean Variable
Suppose we only need to create an EBS volume in the `dev` environment. Firstly, we create an input variable to control this in our `vars.tf`:
```
variable "create-ebs-volume" {
  description = "A boolean to inidcate whether or not the EBS volume should be created."
}
```

Next, we set the `tfvars/dev/terraform.tfvars` for the variable:
```
create-ebs-volume = "true"
```

And also the `tfvars/prod/terraform.tfvars`:
```
create-ebs-volume = "false"
```

# Count Expression
With the boolean variable configured we can then use the `count` expression to control whether or not to create the EBS volume:
```
resource "aws_ebs_volume" "my-ebs-volume" {
  availability_zone = "us-east-1a"
  count             = var.create-ebs-volume == "true" ? 1 : 0
  size              = var.size
  tags = {
    Name = "my-ebs-volume"
  }
}
```

# Dev Apply
With this in place an apply in dev will create the EBS volume:
```
$ export ENVIRONMENT=dev
$ ./tf init
...
$ ./tf apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_ebs_volume.my-ebs-volume[0] will be created
  + resource "aws_ebs_volume" "my-ebs-volume" {
      + arn               = (known after apply)
      + availability_zone = "us-east-1a"
      + encrypted         = (known after apply)
      + id                = (known after apply)
      + iops              = (known after apply)
      + kms_key_id        = (known after apply)
      + size              = 8
      + snapshot_id       = (known after apply)
      + tags              = {
          + "Name" = "my-ebs-volume"
        }
      + type              = (known after apply)
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_ebs_volume.my-ebs-volume[0]: Creating...
aws_ebs_volume.my-ebs-volume[0]: Still creating... [10s elapsed]
aws_ebs_volume.my-ebs-volume[0]: Creation complete after 12s [id=vol-0f69e58a65b402efb]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
```

# Prod Apply
And when running in prod, no EBS volume will be created.
```
$ export ENVIRONMENT=prod
$ ./tf init
...
$ ./tf apply

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.
```
