# Introduction
When you create resources in one Terraform stack you can make their properties available to other Terraform stacks via [Output Values](https://www.terraform.io/docs/configuration/outputs.html).

# Stack 1 - DynamoDB Table
For our first stack we will create a [DynamoDB Table](https://www.terraform.io/docs/providers/aws/r/dynamodb_table.html). In the `stack-1-dynamodb-table` sub-folder we have in our `foo.tf`:
```
resource "aws_dynamodb_table" "my-table" {
  name           = "${var.my-table-name-prefix}-${random_string.result}"
  hash_key       = "AgentId"
  attribute {
    name = "AgentId"
    type = "N"
  }
}
```

We can make attributes of this resource available in outputs by creating an `outputs.tf` file with the following content:
```
output "my-table-name" {
  value = aws_dynamodb_table.my-table.name
}

output "my-table-hash-key" {
  value = aws_dynamodb_table.my-table.hash_key
}
```

We name our outputs (in this example we have named one `my-table-name`) and the value of the output is the [resource address](https://www.terraform.io/docs/commands/state/addressing.html).[resource attribute]. So in this example our output value is `aws_dynamodb_table.my-table.name`. We have another output named `my-table-hash-key` referencing the hash key of the table: `aws_dynamodb_table.my-table.hash_key`.

When we apply this stack we see the outputs listed:
```
$ export ENVIRONMENT=dev
$ cd stack-1-dynamodb-table
$ ./tf init
...
$ ./tf apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_dynamodb_table.my-table will be created
  + resource "aws_dynamodb_table" "my-table" {
      + arn              = (known after apply)
      + billing_mode     = "PAY_PER_REQUEST"
      + hash_key         = "AgentId"
      + id               = (known after apply)
      + name             = (known after apply)
      + stream_arn       = (known after apply)
      + stream_label     = (known after apply)
      + stream_view_type = (known after apply)

      + attribute {
          + name = "AgentId"
          + type = "N"
        }

      + point_in_time_recovery {
          + enabled = (known after apply)
        }

      + server_side_encryption {
          + enabled     = (known after apply)
          + kms_key_arn = (known after apply)
        }
    }

  # random_string.table-name will be created
  + resource "random_string" "table-name" {
      + id          = (known after apply)
      + length      = 16
      + lower       = true
      + min_lower   = 0
      + min_numeric = 0
      + min_special = 0
      + min_upper   = 0
      + number      = true
      + result      = (known after apply)
      + special     = false
      + upper       = true
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

random_string.table-name: Creating...
random_string.table-name: Creation complete after 0s [id=FfcCpFcfqVVuQL6m]
aws_dynamodb_table.my-table: Creating...
aws_dynamodb_table.my-table: Still creating... [10s elapsed]
aws_dynamodb_table.my-table: Creation complete after 11s [id=dev-FfcCpFcfqVVuQL6m]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

Outputs:

my-table-hash-key = AgentId
my-table-name = dev-FfcCpFcfqVVuQL6m
```

# Stack 2 - DynamoDB Table Item
Now that we have our first stack created with outputs, we can create our second stack and reference the DynamoDB table attributes that were output in the first stack. In our 2nd stack we will create a [DynamoDB Table Item](https://www.terraform.io/docs/providers/aws/r/dynamodb_table_item.html) which will be added to the table created in the first stack.

In order to reference outputs from other stacks we must define a [Data Source](https://www.terraform.io/docs/configuration/data-sources.html) that points to the remote state of the stack we want to reference. In our `stack-2-dynamodb-table-item` sub-folder we have the following data block in our `foo.tf` file:
```
data "aws_caller_identity" "current" {}

data "terraform_remote_state" "stack-1-dynamodb-table" {
  backend = "s3"
  config = {
    bucket = "terraform-state-bucket-${data.aws_caller_identity.current.account_id}-dev"
    key    = "stack-1-dynamodb-table/terraform.tfstate"
    region = "us-east-1"
  }
}
```

We use the [aws_caller_identity](https://www.terraform.io/docs/providers/aws/d/caller_identity.html) data source to retrieve details about the AWS account we are running against. We use this data block to retrieve the AWS account id via `data.aws_caller_identity.current.account_id` which is used in the `terraform_remote_state` data block. In this remote state data block we add configuration for the location in S3 of the remote state for our first stack.

With the remote state data block configured we can then reference any output from this stack by using `data.terraform_remote_state.stack-1-dynamodb-table.outputs.OUTPUT_NAME`. In our `foo.tf` example we use this for the `table_name` and `hash_key` attributes of the `aws_dynamodb_table_item` resource:
```
resource "aws_dynamodb_table_item" "my-table-item" {
  table_name = data.terraform_remote_state.stack-1-dynamodb-table.outputs.my-table-name
  hash_key   = data.terraform_remote_state.stack-1-dynamodb-table.outputs.my-table-hash-key

  item = <<ITEM
{
  "AgentId": {"N": "007"},
  "FirstName": {"S": "James"},
  "LastName": {"S": "Bond"}
}
ITEM
}
```
We can then apply the 2nd stack:
```
$ cd stack-2-dynamodb-table-item
$ ./tf init
...
$ ./tf apply
data.aws_caller_identity.current: Refreshing state...
data.terraform_remote_state.stack-1-dynamodb-table: Refreshing state...

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_dynamodb_table_item.my-table-item will be created
  + resource "aws_dynamodb_table_item" "my-table-item" {
      + hash_key   = "AgentId"
      + id         = (known after apply)
      + item       = jsonencode(
            {
              + AgentId   = {
                  + N = "007"
                }
              + FirstName = {
                  + S = "James"
                }
              + LastName  = {
                  + S = "Bond"
                }
            }
        )
      + table_name = "dev-0IIYAX4Z5Asn5bCD"
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_dynamodb_table_item.my-table-item: Creating...
aws_dynamodb_table_item.my-table-item: Creation complete after 0s [id=dev-0IIYAX4Z5Asn5bCD|AgentId|||7]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
```
