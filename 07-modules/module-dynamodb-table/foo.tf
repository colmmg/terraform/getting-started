provider "aws" {
  region = "us-east-1"
}

resource "random_string" "table-name" {
  length = 16
  special = false
}

resource "aws_dynamodb_table" "my-table" {
  billing_mode   = "PAY_PER_REQUEST"
  name           = "${var.my-table-name-prefix}-${random_string.table-name.result}"
  hash_key       = "AgentId"
  attribute {
    name = "AgentId"
    type = "N"
  }
}
