# Introduction
In previous sections we used Terrafrom environment variables to dynamically control our backend and the location of our terraform variables. Building upon this, we can create a wrapper script that can export these Terraform environment variables for us.

# Prerequisites
Before continuing with this example, we need to create the production state bucket which can be created with the following command:
```
$ aws s3 mb s3://terraform-state-bucket-$AWS_ACCOUNT_ID-prod --region us-east-1
```

# The 'tf' script
Our `tf` wrapper script will perform the following:
- get the terraform action from the arguments provided (e.g. `init` or `apply`);
- build the backend configuration based on the separate environment variable `ENVIRONMENT`;
- build the path to the tfvars based on the separate environment variable `ENVIRONMENT`;
- export terraform environment variables for `TF_CLI_ARGS_init` and `TF_CLI_ARGS_apply`;
- run the terraform action

This `tf` script has the following content:
```
#!/bin/bash

# Get terraform action
TF_ACTION="$1"

# Build backend state configuration
TF_STATE_BUCKET="terraform-state-bucket-$AWS_ACCOUNT_ID-$ENVIRONMENT"
TF_STATE_KEY="$(basename `pwd`)/terraform.tfstate"

# Build tfvars path
TF_VARS_PATH=tfvars/$ENVIRONMENT

# Export Terraform environment variables
export TF_CLI_ARGS_init="-backend-config=bucket=$TF_STATE_BUCKET \
                         -backend-config="key=$TF_STATE_KEY" \
                         -backend-config=region=us-east-1"
export TF_CLI_ARGS_apply="-var-file=$TF_VARS_PATH/terraform.tfvars"
export TF_CLI_ARGS_destroy="-var-file=$TF_VARS_PATH/terraform.tfvars"

# Remove existing terraform folder if init
if [ "$TF_ACTION" == "init" ]; then
  rm -rf .terraform
fi

# Run Terraform
terraform $TF_ACTION
```

# The 'Environment' Variable
As mentioned in the previous section we need to set an environment variable of `ENVIORNMENT`. By changing this one variable we can let the `tf` script handle the exporting of all other variables that depend on this so that we have a simple means of running Terraform across multiple environments.

# Sample Run
To fully demonstrate the script, let's try executing Terraform against the dev and prod environments. We can do this by issuing the following set of commands:

### Set ENVIRONMENT=dev
```
$ export ENVIRONMENT=dev
```

### Execute Terraform Against Dev
```
$ ./tf init
$ ./tf apply
```

### Set ENVIRONMENT=prod
```
$ export ENVIRONMENT=prod
```

### Execute Terraform Against Prod
```
$ ./tf init
$ ./tf apply
```
