# Introduction
There is a limitation in the `backend.tf` configuration in a multi-account setup where we have a different S3 bucket per account to store our remote state. Backend configs do not support [Terraform Input Variables](https://www.terraform.io/docs/configuration/variables.html) so we would need to create separate `backend.tf` files per account.

E.g. let's say our `dev` account state bucket was `terraform-state-bucket-123456789012-dev` and our `production` account state bucket was `terraform-state-bucket-123456789012-prod`. Then our `backend.tf` might look like this:
```
terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-123456789012-dev"
    key    = "03-dynamic-backend/terraform.tfstate"
    region = "us-east-1"
  }
}
```

But the `bucket` attribute would have to be modified to `terraform-state-bucket-123456789012-prod` each time we wanted to run Terraform against our production account.

To get around this limitation we use dynamic backend configuration via [Terraform Environment Variables](https://www.terraform.io/docs/commands/environment-variables.html).

# Terraform Init Environment Variables
The `backend.tf` is read during a `terraform init`. When running `terraform init` you may optionally provide extra arguments to dynamically define the backend configuration. This can be done via:
```
$ terraform init \
  -backend-config=bucket=terraform-state-bucket-$AWS_ACCOUNT_ID-dev \
  -backend-config="key=03-dynamic-backend/terraform.tfstate" \
  -backend-config=region=us-east-1
```

However, it is not elegant to have to type these arguments every time. So we make use of Terraform Environment Variables to define the arguments we want to include with a `terraform init` command. The environment variable can be set via:
```
$ export TF_CLI_ARGS_init="-backend-config=bucket=terraform-state-bucket-$AWS_ACCOUNT_ID-dev \
                           -backend-config="key=03-dynamic-backend/terraform.tfstate" \
                           -backend-config=region=us-east-1"
```

With this environment variable set, we can simply run `terraform init` as normal and Terraform will use the backend state defined in our environment variable:
```
$ terraform init
```

Switching to production is then just a matter of exporting the `TF_CLI_ARGS_init` variable again, but this time with `terraform-state-bucket-$AWS_ACCOUNT_ID-prod` as the value of the bucket. E.g.
```
$ export TF_CLI_ARGS_init="-backend-config=bucket=terraform-state-bucket-$AWS_ACCOUNT_ID-prod \
                           -backend-config="key=03-dynamic-backend/terraform.tfstate" \
                           -backend-config=region=us-east-1"
```

### The Backend File
When we use Terraform environment variables in this way, our `backend.tf` becomes simplified:
```
terraform {
  backend "s3" {}
}
```
