# Introduction
By default, Terraform stores state locally in a file named terraform.tfstate. When working with Terraform in a team, use of a local file makes Terraform usage complicated because each user must make sure they always have the latest state data before running Terraform and make sure that nobody else runs Terraform at the same time.

With [remote state](https://www.terraform.io/docs/state/remote.html), Terraform writes the state data to a remote data store, which can then be shared between all members of a team. 

Terraform supports storing the state in Amazon S3.

# S3 Remote State
We define the S3 remote state with a [backend.tf](https://www.terraform.io/docs/backends/types/s3.html) file. In this example we have a `backend.tf` with the following content:
```
terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-123456789012-dev"
    key    = "02-s3-state/terraform.tfstate"
    region = "us-east-1"
  }
}
```

# Prerequisites
In the above backend configuration we have a string of `123456789012` which is a placeholder for your [AWS Account ID](https://docs.aws.amazon.com/IAM/latest/UserGuide/console_account-alias.html). We use this string to ensure we are using a globally unique name for our Terraform state bucket. We need to replace this string with our AWS account id. But we will use this account id extensively as we continue with these tutorials, so let's set it as an environment variable by running the following command:
```
$ export AWS_ACCOUNT_ID=`aws sts get-caller-identity | jq .Account |xargs`
```

Now we can update the account id placeholder in the `backend.tf` file by running the following command:
```
$ sed -i "s,123456789012,$AWS_ACCOUNT_ID,g" backend.tf
```

We also need to create this bucket which you can do by running the following [AWS CLI](https://aws.amazon.com/cli/) command: 
```
$ aws s3 mb s3://terraform-state-bucket-$AWS_ACCOUNT_ID-dev --region us-east-1
```

# Terraform Apply
If you run `terraform init` followed by `terraform apply` you will see that unlike the previous example, there is no local `terraform.tfstate` file created. Instead, Terraform creates and stores it in S3 in the bucket and key defined in our `backend.tf` file.

We can verify the state is present by querying S3 via:
```
$ aws s3 ls s3://terraform-state-bucket-$AWS_ACCOUNT_ID-dev/02-s3-state/
```

# Terraform State
The [terraform state](https://www.terraform.io/docs/commands/state/index.html) can be used to retrieve information from the remote state and to make modifications to it.

### terraform state list
The [terraform state list](https://www.terraform.io/docs/commands/state/list.html) command is used to list resources within a state.
```
$ terraform state list
aws_ebs_volume.my-ebs-volume
```

### terraform state show
The [terraform state show](https://www.terraform.io/docs/commands/state/show.html) command is used to show the attributes of a single resource in the state. The argument for this command is the [resource address](https://www.terraform.io/docs/commands/state/addressing.html) which can be found from `terraform state list`.
```
$ terraform state show aws_ebs_volume.my-ebs-volume
# aws_ebs_volume.my-ebs-volume:
resource "aws_ebs_volume" "my-ebs-volume" {
    arn               = "arn:aws:ec2:us-east-1:123456789012:volume/vol-1f52fb5f221662924"
    availability_zone = "us-east-1a"
    encrypted         = false
    id                = "vol-1f52fb5f221662924"
    iops              = 100
    size              = 8
    tags              = {
        "Name" = "my-ebs-volume"
    }
    type              = "gp2"
}
```

### terraform state rm
The terraform state rm command is used to remove items from the state. When removed from the state, Terraform will stop managing those resources. Like `terraform state show`, the argument for this command is the resource address.
```
$ terraform state rm aws_ebs_volume.my-ebs-volume
Removed aws_ebs_volume.my-ebs-volume
Successfully removed 1 resource instance(s).
```

# Terraform Import
The [terraform import](https://www.terraform.io/docs/commands/import.html) command is used to import existing resources into Terraform. It could be considered the opposite function of `terraform state rm`. It is useful to use this command when resources are created outside of Terraform that you now want to manage within Terraform.

The arguments for this command vary by resource. Note: some resources can't be imported. It is best to review the [documentation](https://www.terraform.io/docs/providers/aws/index.html) for the resource you want to manage. The documentation will usually provide a guide to importing at the bottom of the page. For example, the [EBS Volume](https://www.terraform.io/docs/providers/aws/r/ebs_volume.html) documentation tells us that we can import with the volume id:
```
$ terraform import aws_ebs_volume.my-ebs-volume vol-1f52fb5f221662924
```
