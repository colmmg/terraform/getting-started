provider "aws" {
  region = "us-east-1"
}

resource "aws_ebs_volume" "my-ebs-volume" {
  availability_zone = "us-east-1a"
  size              = 8
  tags = {
    Name = "my-ebs-volume"
  }
}
