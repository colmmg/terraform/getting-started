provider "aws" {
  region = "us-east-1"
}

resource "aws_ebs_volume" "my-ebs-volume" {
  availability_zone = "us-east-1a"
  count             = var.create-ebs-volume == "true" ? 1 : 0
  size              = var.size
  tags = {
    Name = "my-ebs-volume"
  }
}
