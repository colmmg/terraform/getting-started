# Introduction
To support a multi-account (multi-environment) setup, we can use [Input Variables](https://www.terraform.io/docs/configuration/variables.html).

# Prerequisites
Before continuing with this example, initialize Terraform with the following:
```
$ export TF_CLI_ARGS_init="-backend-config=bucket=terraform-state-bucket-$AWS_ACCOUNT_ID-dev \
                           -backend-config="key=04-variables/terraform.tfstate" \
                           -backend-config=region=us-east-1"
$ terraform init
```

# Input Variables
### vars.tf
Variables must be defined in a `vars.tf` file in the root of your working directory. In this example we define a variable for the size of our EBS volume. The following is the content of our `vars.tf` file:
```
variable "size" {
  description = "The size of the EBS volume."
}
```

### terraform.tfvars
Terraform will automatically read the values for input variables from a `terraform.tfvars` file in the root of your working directory. In this example we define the value for our EBS volume. The following is the content of our `terraform.tfvars` file:
```
size = 8
```

### variable referencing
Variables can be referenced with `var.VARIABLE_NAME`. E.g. our updated resource block for the ebs volume in the `foo.tf` file looks like this:
```
resource "aws_ebs_volume" "my-ebs-volume" {
  availability_zone = "us-east-1a"
  size              = var.size
  tags = {
    Name = "my-ebs-volume"
  }
}
```

# Multi-Environment Variables
As stated earlier, Terraform will automatically read the values for input variables from a `terraform.tfvars` file in the root of your working directory. In a multi-environment setup, we will have different `terraform.tfvars` files for each environment. E.g. in this example, maybe we want to have an EBS volume size of 20 in production but only 8 in dev.

### tfvars
To support a multi-environment setup, we will have a `tfvars` folder and within it a subfolder for each environment and within that environment subfolder there will be a `terraform.tfvars` file with the variable values for that environment. The `tree` for this folder is:
```
$ tree tfvars/
tfvars/
├── dev
│   └── terraform.tfvars
└── prod
    └── terraform.tfvars

2 directories, 2 files
```

### var-file argument
Now we need to tell Terraform where to find the variable values for this environment. We can do this by adding the `-var-file` argument in a [terraform apply](https://www.terraform.io/docs/commands/apply.html). In our example, the `terraform apply` command would look like the following for an apply in the dev environment:
```
$ terraform apply -var-file=tfvars/dev/terraform.tfvars
```

### TF_CLI_ARGS
Like we seen in the last section, we can avoid adding this `-var-file` argument each time we run `terraform apply` by exporting [Terraform Environment Variables](https://www.terraform.io/docs/commands/environment-variables.html). So by running the following, we instruct Terraform that we want to use the dev variables:
```
$ export TF_CLI_ARGS_apply="-var-file=tfvars/dev/terraform.tfvars"
```

Then to apply the configuration we can simply run:
```
$ terraform apply
```

Switching to production variables can be done by exporting the `TF_CLI_ARGS_apply` variable again with the path to the production variables:
```
$ export TF_CLI_ARGS_apply="-var-file=tfvars/prod/terraform.tfvars"
```
