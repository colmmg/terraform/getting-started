# Introduction
This repository provides a set of tutorials to help you get started with [Terraform](https://www.terraform.io/) in [Amazon Web Services (AWS)](https://aws.amazon.com/).

It also demonstrates how Terraform can be adapted to facilitate a multi-environment (multi-account) configuration.

# AWS Account
To follow along in the tutorials you will need an AWS account. If you do not have one you can create one by following the instructions at [Creating an AWS Account](https://docs.aws.amazon.com/AmazonSimpleDB/latest/DeveloperGuide/AboutAWSAccounts.html).

# AWS Cloud9
We will use the [AWS Cloud9](https://aws.amazon.com/cloud9/) service for these tutorials. Follow the steps below to configure a new Cloud9 IDE in your account.

### Create Environment
In the Cloud9 service, create a new environment. You can accept all the defaults in the creation wizard. After a few minutes your IDE will be ready.

### Download Terraform
When your IDE is ready, click on Window > New Terminal and install Terraform via:
```
$ curl -o /tmp/terraform.zip "https://releases.hashicorp.com/terraform/0.12.20/terraform_0.12.20_linux_amd64.zip" && \
    sudo unzip -d /usr/local/bin/ /tmp/terraform.zip && \
    sudo chmod +x /usr/local/bin/terraform
```

### Clone Repo
Clone this repository into your environment via:
```
$ git clone https://gitlab.com/colmmg/terraform/getting-started.git ~/getting-started && cd ~/getting-started
```

### Install jq
Install `jq` with:
```
$ sudo yum install -y jq
```
