terraform {
  backend "s3" {
    bucket = "terraform-state-bucket-123456789012-dev"
    key    = "02-s3-state/terraform.tfstate"
    region = "us-east-1"
  }
}
