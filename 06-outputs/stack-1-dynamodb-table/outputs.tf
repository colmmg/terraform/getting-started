output "my-table-name" {
  value = aws_dynamodb_table.my-table.name
}

output "my-table-hash-key" {
  value = aws_dynamodb_table.my-table.hash_key
}
