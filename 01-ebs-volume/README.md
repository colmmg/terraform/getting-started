# Introduction
This section provides an introduction to [Terraform](https://www.terraform.io/) by demonstrating how we can define an [Amazon Elastic Block Store (EBS) Volume](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-volumes.html) with Terraform.

# Code
Terraform code can be defined in any file that has a `.tf` extension. In this example we have defined our code in `foo.tf`.

### Providers
A [provider](https://www.terraform.io/docs/providers/index.html) is responsible for understanding API interactions and exposing resources. At the top of our `foo.tf` we have defined our [Amazon Web Services (AWS) provider](https://www.terraform.io/docs/providers/aws/index.html).
```
provider "aws" {
  region = "us-east-1"
}
```

### Resources
Using the documentation for the [Amazon Web Services (AWS) provider](https://www.terraform.io/docs/providers/aws/index.html) we can define the resources we want to manage with Terraform. In this example we will refer to the [EBS Volume](https://www.terraform.io/docs/providers/aws/r/ebs_volume.html) documentation to define an EBS volume in our `foo.tf` file.
```
resource "aws_ebs_volume" "my-ebs-volume" {
  availability_zone = "us-east-1a"
  size              = 8
  tags = {
    Name = "my-ebs-volume"
  }
}
```

# Terraform Init
The [terraform init](https://www.terraform.io/docs/commands/init.html) command is used to initialize a working directory containing Terraform configuration files.
```
$ terraform init

Initializing the backend...

Initializing provider plugins...
- Checking for available provider plugins...
- Downloading plugin for provider "aws" (hashicorp/aws) 2.48.0...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.aws: version = "~> 2.48"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

After running this, you will see that a `.terraform` folder has been created in your working directory.

# Terraform Apply
The [terraform apply](https://www.terraform.io/docs/commands/apply.html) command is used to apply the changes required to reach the desired state of the configuration.
```
$ terraform apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_ebs_volume.my-ebs-volume will be created
  + resource "aws_ebs_volume" "my-ebs-volume" {
      + arn               = (known after apply)
      + availability_zone = "us-east-1a"
      + encrypted         = (known after apply)
      + id                = (known after apply)
      + iops              = (known after apply)
      + kms_key_id        = (known after apply)
      + size              = 8
      + snapshot_id       = (known after apply)
      + tags              = {
          + "Name" = "my-ebs-volume"
        }
      + type              = (known after apply)
    }

Plan: 1 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: 
```

At the prompt, enter 'yes' to apply the changes.
```
  Enter a value: yes

aws_ebs_volume.my-ebs-volume: Creating...
aws_ebs_volume.my-ebs-volume: Still creating... [10s elapsed]
aws_ebs_volume.my-ebs-volume: Creation complete after 12s [id=vol-1f52fb5f221662924]

Apply complete! Resources: 1 added, 0 changed, 0 destroyed.
```

# Terraform State File
Terraform must store state about your managed infrastructure and configuration. This state is used by Terraform to map real world resources to your configuration and keep track of metadata.

When you run `terraform apply`, the [State](https://www.terraform.io/docs/state/index.html) is tracked in a newly created `terraform.tfstate` file. If you view the contents of this file you will see something like the following:
```
{
  "version": 4,
  "terraform_version": "0.12.13",
  "serial": 1,
  "lineage": "c114abf7-2d87-b56e-b492-dad50e5703e5",
  "outputs": {},
  "resources": [
    {
      "mode": "managed",
      "type": "aws_ebs_volume",
      "name": "my-ebs-volume",
      "provider": "provider.aws",
      "instances": [
        {
          "schema_version": 0,
          "attributes": {
            "arn": "arn:aws:ec2:us-east-1:123456789012:volume/vol-1f52fb5f221662924",
            "availability_zone": "us-east-1a",
            "encrypted": false,
            "id": "vol-1f52fb5f221662924",
            "iops": 100,
            "kms_key_id": "",
            "size": 8,
            "snapshot_id": "",
            "tags": {
              "Name": "my-ebs-volume"
            },
            "type": "gp2"
          },
          "private": "bnVsbA=="
        }
      ]
    }
  ]
}
```

# Terraform Destroy
The [terraform destroy](https://www.terraform.io/docs/commands/destroy.html) command is used to destroy the Terraform-managed infrastructure.
```
$ terraform destroy
aws_ebs_volume.my-ebs-volume: Refreshing state... [id=vol-1f52fb5f221662924]

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  - destroy

Terraform will perform the following actions:

  # aws_ebs_volume.my-ebs-volume will be destroyed
  - resource "aws_ebs_volume" "my-ebs-volume" {
      - arn               = "arn:aws:ec2:us-east-1:123456789012:volume/vol-1f52fb5f221662924" -> null
      - availability_zone = "us-east-1a" -> null
      - encrypted         = false -> null
      - id                = "vol-1f52fb5f221662924" -> null
      - iops              = 100 -> null
      - size              = 8 -> null
      - tags              = {
          - "Name" = "my-ebs-volume"
        } -> null
      - type              = "gp2" -> null
    }

Plan: 0 to add, 0 to change, 1 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: 
```

At the prompt, enter 'yes' to destroy the resources.
```
  Enter a value: yes

aws_ebs_volume.my-ebs-volume: Destroying... [id=vol-1f52fb5f221662924]
aws_ebs_volume.my-ebs-volume: Destruction complete after 1s

Destroy complete! Resources: 1 destroyed.
```
