# Introduction
When you need to reuse Terraform code, instead of duplicating that code across many stacks you may take advantage of [Terraform Modules](https://www.terraform.io/docs/configuration/modules.html). Modules allow you to define your code once and reference it across multiple stacks. You can even have some degree of flexibility by exposing inputs to the module which can adjust the configuration.

# DynamoDB Table Module
For this example we have a module named `module-dynamodb-table` that will define our configuration for a DynamoDB table. The content of this module is just the resources in `foo.tf` and the module's input variables are in `vars.tf`.

# Stack Using The Module
Now that we have our module defined we can use it in other stacks. In the `stack-using-module` sub-folder we reference the module in the `foo.tf` file with:
```
module "dynamodb-table" {
  my-table-name-prefix = var.my-table-name-prefix
  source               = "../module-dynamodb-table"
}
```

We name our reference to the module `dynamodb-table` and we instruct Terraform where the module is stored via the `source` argument. Input variables for the module are also passed as arguments and in this case we pass in the input of `my-table-name-prefix`.

We can then apply the stack:
```
$ cd stack-using-module
$ ./tf init
...
$ ./tf apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.dynamodb-table.aws_dynamodb_table.my-table will be created
  + resource "aws_dynamodb_table" "my-table" {
      + arn              = (known after apply)
      + billing_mode     = "PAY_PER_REQUEST"
      + hash_key         = "AgentId"
      + id               = (known after apply)
      + name             = (known after apply)
      + stream_arn       = (known after apply)
      + stream_label     = (known after apply)
      + stream_view_type = (known after apply)

      + attribute {
          + name = "AgentId"
          + type = "N"
        }

      + point_in_time_recovery {
          + enabled = (known after apply)
        }

      + server_side_encryption {
          + enabled     = (known after apply)
          + kms_key_arn = (known after apply)
        }
    }

  # module.dynamodb-table.random_string.table-name will be created
  + resource "random_string" "table-name" {
      + id          = (known after apply)
      + length      = 16
      + lower       = true
      + min_lower   = 0
      + min_numeric = 0
      + min_special = 0
      + min_upper   = 0
      + number      = true
      + result      = (known after apply)
      + special     = false
      + upper       = true
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.dynamodb-table.random_string.table-name: Creating...
module.dynamodb-table.random_string.table-name: Creation complete after 0s [id=QaonVdIP2blxCF4h]
module.dynamodb-table.aws_dynamodb_table.my-table: Creating...
module.dynamodb-table.aws_dynamodb_table.my-table: Creation complete after 7s [id=dev--QaonVdIP2blxCF4h]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.
```
