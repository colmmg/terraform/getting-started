provider "aws" {
  region = "us-east-1"
}

module "dynamodb-table" {
  my-table-name-prefix = var.my-table-name-prefix
  source               = "../module-dynamodb-table"
}
